#!/usr/bin/perl
use strict;
use warnings;
use v5.32;
use DBI;
use Time::Piece; #REMOVE
use Time::HiRes;
use Try::Tiny;
use IPC::System::Simple qw(system systemx capture capturex $EXITVAL EXIT_ANY);
use POSIX qw(mkfifo);
use Cwd;
$SIG{'QUIT'} = 'IGNORE'; #to prevent system() from killing a process when SIGQUIT is recieved from keyboard

my $VERSION_NUMBER = "3.24.1 beta";

sub cpu_test;
sub time_sync;
sub smart_test;
sub select_drives;
sub gather_data;
sub generate_report;
sub get_drive_status;
sub extract;
sub grade_string;
sub print_report;
sub hash_parse;
sub ticking_clock;
sub network_configurator;
sub strip_special_characters;
sub check_partitions;
sub initiate_connection;
sub check_updates;

my $USER='triage';
my $HOST='192.53.161.98';
my $DATABASE="Morris";

system("clear");
system("echo triage | figlet");
system("echo $VERSION_NUMBER | lolcat");

if ($> != 0){
    die ("This tool requires root");
}

my %system_info = gather_data;

initiate_connection; #Inserts system into pending table, also initiates network uplink
#check_updates;

my $stress_pid = fork;
if ($stress_pid == 0) {
    cpu_test;
    exit;
}

battery_test();

my @drive_list=select_drives;
smart_test(@drive_list);
sanitize(@drive_list);
say "Checking if disks are responsive...";
capturex(qw(hdparm -I), $_->{PATH}) for @drive_list;
check_partitions(@drive_list);
sample(@drive_list); #Verify that sanitize worked

say "Waiting on stress...";
waitpid($stress_pid, 0);

system(qw(systemctl start gpm.service));
system('./triage/endscreen', $system_info{serial});
my $id = generate_report(\%system_info, \@drive_list);

system("figlet $system_info{serial} | lolcat");
system("figlet $id | lolcat");
print "Press ENTER to shutdown";
<STDIN>;
system('shutdown now');

#local space for db connection management
sub initiate_connection{
    my $system_ref = \%system_info;
    my $dsn;
    my $dbh;
    while ('true'){
	$dsn = "DBI:MariaDB:database=$DATABASE;host=$HOST;mariadb_connect_timeout=0";
	$dbh = DBI->connect("$dsn", "$USER", q(famcserver), { RaiseError => 0, PrintError=> 1}) or do {
	    network_configurator;
	    next;
	};
	last;
    }
    
    my $statement_prototype = q(INSERT INTO pending (manufacturer, model, serial, cpu_model, cpu_speed, ram)
       			      	       	    	      VALUES    (?,	     ?,	    ?,	    ?,	       ?,	  ?  ));
    my @bind_values = ($system_ref->{manufacturer}, $system_ref->{model}, $system_ref->{serial},
		       $system_ref->{cpu_model}, $system_ref->{cpu_speed}, $system_ref->{memory});
    my $sth = $dbh->prepare($statement_prototype);
    $sth->execute(@bind_values);

    $dbh->disconnect;
}


sub sample{
    my @disk_hashes = @_;
    foreach my $reference (@disk_hashes){
	my $sampler_code;
	try {
	    system("./triage/sampler $reference->{PATH}");
	}
	catch {
	    if ($EXITVAL == 1){die "Unspecified error, see sampler output"}
	    if ($EXITVAL == 2){
		$reference->{sampler_result} = "Garbage";
		warn("Warning: data appears to be garbage");
		say("The system is unable to be verrified");
		print("Press enter to continue");
		my $answer = <STDIN>;
	    }
	    elsif ($EXITVAL == 3){
		$reference->{sampler_result} = "Failed";
		while(1){
		    say("Warning: disk is unclean and must be removed before sale");
		    say("Please type \"I understand\" to continue");
		    my $answer = <STDIN>;
		    chomp $answer;
		    #answer can be EOF (ctrl-d) in the event keyboard is broken
		    unless( $answer == "I understand" || !$answer){ next } 
		    else{ last }
		}
	    }	    
	};
	if ($EXITVAL == 0){ $reference->{sampler_result} = "Passed" }
	$reference->{sampler_version} = capture("./triage/sampler --version");
	chomp $reference->{sampler_version};
    }
}

sub print_report{
    system('clear');
    my $sys_ref = $_[0];
    my $filename = "$sys_ref->{manufacturer}".'_'."$sys_ref->{serial}";
    system("figlet $sys_ref->{serial} | lolcat");
    print "Would you like to print? [y] ";
    my $answer = <STDIN>;
    chomp $answer;
    #    unless (lc($answer) eq 'n') { ssh->cmd("lp ~/logs/${filename}.pdf") }
}   

sub cpu_test{
    my $cores;
    open(my $CPU_INFO, '<', '/proc/cpuinfo') || die "popen: $!";
    while(<$CPU_INFO>) {
	unless (/siblings/) { next }
	s/siblings\s+:\s+//;
	$cores = $_;
	last
    }
    chomp $cores;
    close($CPU_INFO);
    capture('stress', '-c', $cores, '--timeout', '10s');
}

sub check_partitions{
    my @drive_refs = @_;
    system("partprobe");
    my @lsblk_output = capture("lsblk -P -o name,path,model,serial,size,type,tran,mountpoint,rota");
    my @partitions = grep { /TYPE="part"/ } @lsblk_output;
    foreach my $reference (@drive_refs){
	say $reference->{PATH};
	if (grep { /$reference->{PATH}/ } @partitions){
	    die "This system still has partitions left over on $reference->{PATH}";
	    #warn "This system still has partitions left over on $reference->{PATH}";
	    #sanitize(@drive_list);
	    
	}
	else { say "Partitions clear" }
    }
}

sub select_drives{
    use autodie;
    my @hash_references;
    my @lsblk_output = capture("lsblk -P -o name,path,model,serial,size,type,tran,mountpoint,rota");
    my @disks = grep { /TYPE="disk"/ && (/TRAN="[mu]?[sp]?ata"/ || /TRAN="nvme"/i || /TRAN=""/) && /MOUNTPOINT=""/ } @lsblk_output;
    s/MODEL=""/MODEL="NULL"/g, s/SERIAL=""/SERIAL="NULL"/g, s/TRAN=""/TRAN="NULL"/g for @disks;
    my @mounted_partitions = grep { /TYPE="part"/ && !/MOUNTPOINT=""/ } @lsblk_output;
    chomp for (@disks, @mounted_partitions);

    foreach (@disks){
	my $hash_ref = hash_parse($_);
	#sanity check on hash
	foreach my $key (qw( NAME PATH MODEL SERIAL SIZE TYPE TRAN )){
	    unless($hash_ref->{$key}){
		    system("lsblk -P -o name,path,model,serial,size,type,tran,mountpoint,rota");
		die "Failed to parse hash reference at $key";
	    }
	}
	unless (grep { /$hash_ref->{NAME}/ } @mounted_partitions){ push @hash_references, $hash_ref }
    }
    foreach my $reference (@hash_references){
	if ($reference->{TRAN} =~ /[mu]?[sp]?ata/){
	    my @hdparm_output = capture("hdparm -I $reference->{PATH}");
	    my ($true_size) = grep { /1000\*1000/ } @hdparm_output;
	    chomp, s/.*\(/\(/ for $true_size;
	    my ($true_rotation) = grep { /Nominal Media Rotation Rate:/ } @hdparm_output;
	    unless ($true_rotation) { $true_rotation = "Unreported" };
	    chomp, s/.*: //, s/Solid State Device/SSD/ for $true_rotation;
	    ($reference->{SIZE}, $reference->{ROTA}) = ($true_size, $true_rotation);
	    $reference->{SMART} = system([0..256], "smartctl -H -q silent $reference->{PATH}");
	    
	    my ($temp_line) = grep { /SECURITY ERASE UNIT/ } @hdparm_output;
	    my @time_lines = split(/\./, $temp_line);
	    my @estimates;
	    push @estimates, split(/\s+/, $_) for @time_lines;
	    @estimates = grep { /min/ } @estimates;
	    s/min// for @estimates;
	    $reference->{ERASE} = $estimates[1] ? $estimates[1] : $estimates[0];
	}
	elsif ($reference->{TRAN} eq 'nvme'){
	    $reference->{ROTA} = 'NVMe';
	    $reference->{ERASE} = 0;
	}
    }
    return @hash_references;
}

sub hash_parse{
    my $line = $_[0];
    my @parts;
    push(@parts, split(/=/, $_)) for split(/"\s+/, $line);
    my %hash = (@parts); 
    s/^"//, s/"$// for (values %hash);
    return \%hash;
}

sub smart_test{	
    my @disk_hashes = @_;
    my @smart_pids;
    foreach my $reference (@disk_hashes){
	my $child_pid = fork;
	if ($child_pid != 0){
	    $reference->{PID} = $child_pid;
	    next;
	}
	else { #Child Block
	    system([0..256], "smartctl --test=short -q silent -C $reference->{PATH}");
	    my $mask = system([0..256], "smartctl -q silent -a $reference->{PATH}");
	    exit $mask;
	}
    }
    #Parent Block
    foreach my $reference (@disk_hashes){ #Wait for all children
	say "Waiting on smart...";
	waitpid($reference->{PID}, 0);
	$reference->{SMART} = $? >> 8; #Bit-shift eight to the right
	if ($reference->{SMART} != 0 && $reference->{SMART} < 16) { die "SMART test has failed on $reference->{PATH}" }
	#unless ($reference->{SMART} == 0){ parse_bitmask($reference) }
    }
}

sub sanitize{	
    my @disk_hashes = @_;
    #unfreeze sata drives
    foreach my $reference (@disk_hashes){
	if ($reference->{TRAN} =~ /[mu]?[sp]?ata/){
	    unless ($reference->{ROTA} eq 'SSD'){
		say "This unit contains a mechanical Hard Disk";
		while ('true'){
		    print "Continue? [y/n] ";
		    my $answer = <STDIN>;
		    chomp $answer;
		    if (lc($answer) eq 'y'){ last }
		    elsif (lc($answer) eq 'n'){ exit }
		    elsif (lc($answer) eq 'skip'){
			$reference->{SAN} = "PreSanitized";
			return;
		    }	
		    else { next }
		}
	    }
	    my @hdparm_output = capture("hdparm -I $reference->{PATH}");
	    unless (grep { /not\s+frozen/ } @hdparm_output){
		#This puts the computer to sleep in order to unfreeze the drives
		system("rtcwake -m no -s 15 && systemctl suspend && sleep 25s");
		@hdparm_output = capture("hdparm -I $reference->{PATH}");
		unless (grep { /not\s+frozen/ } @hdparm_output){
		    warn "$reference->{PATH} failed to unfreeze";
		}
	    }
	}
    }

    foreach my $reference (@disk_hashes){
	my $child_pid = fork;
	if ($child_pid){
	    $reference->{PID} = $child_pid;
	    next;
	}
	else { #Child Block
	    my $fifo_path = '/tmp/'.$reference->{NAME}.'.fifo';
	    mkfifo($fifo_path, 0755);
	    my $san_method;
	    if ($reference->{TRAN} =~ /[mu]?[sp]?ata/){ sata_wiper($reference) }
	    elsif ($reference->{TRAN} eq 'nvme'){ nvme_wiper($reference) } #NVMe WIPE, unimplemented
	    #elsif ($reference->{TRAN} eq ''){ die "Unit has unidentified transfer type" }
	    else { die "Unidentified transfer type in sanitize $reference->{PATH}" }
	    exit 0;
	}
    }
    #Parent Block
    my $wipe_time = 0;
    foreach my $reference (@disk_hashes){if ($reference->{ERASE} > $wipe_time) {$wipe_time = $reference->{ERASE}}}
    if (@disk_hashes){
	unless ($wipe_time == 0){ say "Operation will take $wipe_time minutes to complete" }
	else { say "Operation will take an unknown length of time to complete" }
    }

    #my $clock_pid = fork;
    #if ($clock_pid == 0){ticking_clock}

    foreach my $reference (@disk_hashes){ #Wait for all children
	say "Waiting on sanitizer...";
	sleep 20;
	if (-e "/proc/$reference->{PID}") {
	    sleep 1 until (-e "/tmp/$reference->{NAME}.fifo"); 
	    open(my $input_pipe, "<", "/tmp/$reference->{NAME}.fifo");
	    $reference->{SAN} = <$input_pipe>;
	    close $input_pipe;	    
	    waitpid($reference->{PID}, 0);
	} else {
	    waitpid($reference->{PID}, 0);
	    unless ($? == 0) { die "$reference->{PATH} failed to sanitize" }
	    $reference->{SAN} = "ERROR";
	}
	if ( $reference->{SAN} =~ /ERROR/ ) { die "$reference->{PATH} failed to sanitize" }
    }
    #kill 1, $clock_pid;
}

sub recall_check{
    my $system_name = capture('dmidecode -s system-product-name');
    chomp, s/ /\%20/g for $system_name;

    my $webstring = "https://www.saferproducts.gov/RestWebServices/Recall?ProductName=${system_name}&ProductModel=${system_name}&format=json";
    my @output = capture(EXIT_ANY, "curl -s ${webstring}");
    my $count = @output;
    return $count;
}

sub battery_test{
	my @battery_info = capture("acpi -ib");
	my @adapter_info = capture("acpi -ia");
	if (grep {/on-line/} @adapter_info){
		if (grep {/zero rate/} @battery_info){ battery_warning() }
	}elsif (grep {/off-line/} @adapter_info){
		unless (grep {/[Dd]ischarging/} @battery_info) { battery_warning() }
	}
}
		
sub battery_warning{
			say("There may be something wrong with the battery");
			my $battery_status = capture('acpi -ib');
			say($battery_status);
			say('Please investigate further');
			say('Press Enter to continue');
			<STDIN>;
}

sub nvme_wiper{
    #warn "NVMe Support is currently experimental, notify administrator";
    my $reference = $_[0];
    open(my $output_pipe, ">", "/tmp/$reference->{NAME}.fifo");
    my $return_code = system(EXIT_ANY, "nvme format $reference->{PATH} --ses=1 --force");
    if ($return_code == 0) { print $output_pipe 'NVMe Format' }
    else { print $output_pipe 'NVMe ERROR' }
    close $output_pipe;
}

sub sata_wiper{
    my $reference = $_[0];
    open(my $output_pipe, ">", "/tmp/$reference->{NAME}.fifo");
    my @hdparm_output = capture(EXIT_ANY, "hdparm -I $reference->{PATH}");
    capture(EXIT_ANY, "hdparm --security-set-pass 'geek' $reference->{PATH}");
    sleep 3;
    unless ( grep { /not\s+supported: enhanced erase/ } @hdparm_output ){
	capture(EXIT_ANY, "hdparm --security-erase-enhanced 'geek' $reference->{PATH}");
	if ($EXITVAL != 0) { print $output_pipe "ERROR" }
	else { print $output_pipe "Enhanced" }
    }
    else {
	capture(EXIT_ANY, "hdparm --security-erase 'geek' $reference->{PATH}");
	if ($EXITVAL != 0) { print $output_pipe "ERROR" }
	else { print $output_pipe "Standard" }
    }
    close $output_pipe;
    print "Complete\n";
}


sub gather_data{
    #Allows access to unicode character names
    use charnames ':full';
    my %system_info;
    $system_info{techName}="Tech Room";

    $system_info{version} = $VERSION_NUMBER;

    $system_info{chassis} = capture("dmidecode -s chassis-type");
    chomp $system_info{chassis};

    my $date = localtime->ymd('/');
    $system_info{date}=$date;
    
    my $manufacturer = `dmidecode -s system-manufacturer`; 
    s/ /\-/g, s;/;\-;g, chomp for $manufacturer; 
    $system_info{manufacturer} = $manufacturer; 

    my $model = capture(qw(dmidecode -s), ($manufacturer eq 'LENOVO' ? 'system-version' : 'system-product-name'));
    #$model =~ s/_/\\_/g; #This filtering is now performed server-side
    chomp $model;
    $system_info{model}=$model;

    my $serial = `dmidecode -s system-serial-number`;
    chomp, s/ /-/g for $serial;
    s/\s+$// for $serial; #edge case with certain ASUS units
    $system_info{serial}=$serial;

    my $cpuinfo = {};
    foreach my $cpuline ( capture("lscpu") ){
	chomp $cpuline if $cpuline;
	my ( $attribute, $value ) = split /:/, $cpuline, 2;
	
	$attribute =~ s/\s+/_/g;
	$attribute = lc($attribute);
	$value =~ s/\s+//;
	$cpuinfo->{$attribute} = $value;
    }
    s/Intel//, s/\(R\)//g, s/\(TM\)//g,
	s/CPU//, s/@.*//, s/^\s+//, s/\s+/ /g for $cpuinfo->{model_name};

    my $cpu_speed = capture('dmidecode -s processor-frequency');
    if ($cpu_speed) {
	$cpu_speed =~ s/\s+MHz//;
	chomp $cpu_speed;
    } else { $cpu_speed = $cpuinfo->{cpu_max_mhz} / 1000 }
    $cpu_speed = $cpu_speed.'.00' if (length($cpu_speed) == 1);
    $cpu_speed = $cpu_speed.'0' if (length($cpu_speed) == 3);
    $cpu_speed = $cpu_speed.'MHz';
    
    ($system_info{cpu_model}, $system_info{cpu_speed}) = ($cpuinfo->{model_name}, $cpu_speed);
    
    my @lspci = capture('lspci');
    $system_info{wireless} = grep {/Network\s+controller:/} @lspci;
    my @gpu = grep(/3D|VGA|Display controller/, @lspci);
    @gpu = grep(/nvidia|amd/i, @gpu);
    chomp for $system_info{gpu} = join(', ', @gpu);

    if (-d '/dev/input/by-id'){
	opendir(my $inputs, '/dev/input/by-id/');
	$system_info{touchscreen} = grep { /Touch_Panel|(?!multi)touch|Tablet_ISD|MosArt/i } (readdir($inputs));
	closedir($inputs);
    }
    
    my @modules = capture('lsmod');
    $system_info{touchscreen} = grep { /(?<!multi)touch/i } @modules unless $system_info{touchscreen};

    unless ($system_info{touchscreen}) {
	if( grep { /hid_multitouch/i } @modules ){
	    say "Does this device have a touchscreen? [y/N]";
	    my $answer = <STDIN>;
	    chomp $answer;
	    if (lc($answer) eq 'y') { $system_info{touchscreen} = 1 }
	}
    }
    
    my $resolution;
    if ( -d '/sys/class/drm' ) {
	foreach my $dev (glob '/sys/class/drm/*/modes') {
	    open(my $MODES, '<', $dev);
	    $resolution = <$MODES>;
	    close $MODES;
	    unless ($resolution) { next; }
	    chomp $resolution;
	    last;
	}
    }
    $system_info{resolution} = $resolution;

    my @memory = capture("lsmem");
    #(my $memory) = grep {/online memory:/} @memory;
    #chomp, /.*memory:\s+/ for $memory;
    extract(\@memory, 'online memory:');
    my $memory = join(' ', @memory);
    $system_info{memory} = $memory;

    my @battery = capture(qw(acpi -ib));
    unless (grep {/zero rate/} @battery){
    	#warn "Battery :\n".join("\n", map '<'.$_.'>', @battery);
    	for my $current_line  (@battery){
		unless($current_line =~ /last full capacity/) { next }
			#warn "Line: $current_line";
			my @batt_info = split(/ /, $current_line);
			$system_info{battery} = $batt_info[-1];
			#$system_info{battery} =~ s/%/\\%/; #This filtering is now server-side
			chomp $system_info{battery};
    		}
    }else{ $system_info{battery} = 'Defect' }    

    my @lsusb = capture('lsusb');
    $system_info{bluetooth} = 0; #defaults to 0
    for my $current_line (@lsusb){
	unless($current_line =~ /Bluetooth/) { next }
	$system_info{bluetooth} = 1;
	last;
    }
    #say $_ for @cpu;
    $system_info{recall_count} = recall_check();
    return %system_info;
}

sub extract {
    my($data, $string) = @_;
    @{$data} = grep {/$string/} @{$data};
    chomp, s/.*$string\s+// for @{$data};
}


sub generate_report {
    my $system_ref = $_[0];
    my @disk_info = @{$_[1]};
    
    #my @capacity;
    #my @rotation;
    #foreach my $i (0 .. (scalar(@disk_info) - 1)) {
    #push(@capacity, $disk_info[$i]->{capacity});
    #push(@rotation, $disk_info[$i]->{rotation});
    #}

    #my $capacity = join(', ', @capacity) if (@capacity);
    #my $rotation = join(', ', @rotation) if (@rotation);
    
    open(my $TEMPLATE, '<', glob './triage/template.tex');
    my @output = <$TEMPLATE>;
    close($TEMPLATE);

    system("clear");
    say "Please select the Cosmetic Category of this system [LVR, HVR]";
    my $cosmetic_grade;
    do {
	print "Please input your selection: ";
	my $answer = <STDIN>;
	chomp $answer;
	if ( $answer =~ m/LVR/i ){ $cosmetic_grade = 'LVR' }
	elsif ( $answer =~ m/HVR/i ){ $cosmetic_grade = 'HVR' }
    } until ($cosmetic_grade);
    my $function_grade;
    if (($system_ref->{battery} || $system_ref->{chassis} =~ m/(desktop|tower)/i ) && @disk_info) {
	say "Please select the Function Category of this system [RI, RC]";
	do {
	    print "Please input your selection: ";
	    my $answer = <STDIN>;
	    chomp $answer;
	    if ( $answer =~ m/RI/i ){ $function_grade = 'RI' }
	    elsif ( $answer =~ m/RC/i ){ $function_grade = 'RC' }
	} until ( $function_grade ); }
    else { $function_grade = 'RI' }
    
    my $supplier_code;
    while(){
	say "Please input a valid supplier code if applicable";
	print "Code: ";
	$supplier_code = <STDIN>;
	$supplier_code =~ s/[^a-zA-Z0-9, ]//g;
	chomp $supplier_code;
	$supplier_code = uc($supplier_code);
	if (length($supplier_code) <= 4) {
	    print "You input \"${supplier_code}\", is this correct? [Y/n]";
	    my $answer = <STDIN>;
	    chomp $answer;
	    if (lc($answer) eq 'n'){ next }
	    else { last }
	}
    }
    my $purchase_number;
    while(){
	say "Please input a valid purchase number if applicable";
	print "Code: ";
	$purchase_number = <STDIN>;
	$purchase_number =~ s/[^a-zA-Z0-9, ]//g;
	chomp $purchase_number;
	$purchase_number = uc($purchase_number);
	print "You input \"${purchase_number}\", is this correct? [Y/n]";
	my $answer = <STDIN>;
	chomp $answer;
	if (lc($answer) eq 'n'){ next }
	else{ last }
    }

    say "***Please add any additional information about this unit below***";
    my $additional_info = <STDIN>;
    $additional_info =~ s/[^a-zA-Z0-9, ]//g;
    
    
    
    open(my $OUTPUT_FILE, '>', 'output.tex');
    print $OUTPUT_FILE @output;
    close($OUTPUT_FILE);

    my $filename = "$system_ref->{manufacturer}_$system_ref->{serial}";

    my $dsn;
    my $dbh;
    try{
	while ('true'){
	    $dsn = "DBI:MariaDB:database=$DATABASE;host=$HOST;mariadb_connect_timeout=0";
	    $dbh = DBI->connect("$dsn", "$USER", q(famcserver), { RaiseError => 0, AutoCommit => 0 }) or do {
		network_configurator;
		next;
	    };
	    last;
	}

	my $statement_prototype = q(INSERT INTO inventory (manufacturer, model, serial, cpu_model, cpu_speed, ram)
       			      	       	    VALUES    (?,	     ?,	    ?,	    ?,	       ?,	  ?  ));
	my @bind_values = ($system_ref->{manufacturer}, $system_ref->{model}, $system_ref->{serial},
			   $system_ref->{cpu_model}, $system_ref->{cpu_speed}, $system_ref->{memory});
	my $sth = $dbh->prepare($statement_prototype);
	$sth->execute(@bind_values);

	my $row_id = $sth->last_insert_id();

	$statement_prototype = q(INSERT INTO extended 
    			   	    	 (id, triage_version, resolution, battery, 
					 wifi, bluetooth, touchscreen, 
					 gpu, additional_notes,
					 cosmetic_grade, function_grade, chassis,
					 supplier_code, purchase_number, recall_count) VALUES
					 (?, ?, ?, ?,
					  ?, ?, ?,
					  ?, ?, ?, 
					  ?, ?,
					  ?, ?, ?));
	@bind_values = ($row_id, $system_ref->{version}, $system_ref->{resolution}, $system_ref->{battery},
			$system_ref->{wireless}, $system_ref->{bluetooth}, $system_ref->{touchscreen},
			$system_ref->{gpu}, $additional_info, $cosmetic_grade, $function_grade, $system_ref->{chassis},
	                $supplier_code, $purchase_number, $system_ref->{recall_count});
	$sth = $dbh->prepare($statement_prototype);
	$sth->execute(@bind_values);

	foreach my $disk_reference (@disk_info){
	    my $hdparm_version = capture("hdparm -V");
	    chomp $hdparm_version;
	    my @hdparm_output = capture("hdparm -I $disk_reference->{PATH}");
	    
	    $statement_prototype = q(INSERT INTO storage (attached_unit, transfer_type, model, serial,
			       		     	      capacity, rotation, sanitize_method, smart_status, 
						      hdparm_version, verification, verifier_version ) VALUES
						      (?, ?, ?, ?, 
						       ?, ?, ?, ?, 
						       ?, ?, ?));
	    @bind_values = ($row_id, $disk_reference->{TRAN}, $disk_reference->{MODEL}, $disk_reference->{SERIAL},
			    $disk_reference->{SIZE}, $disk_reference->{ROTA}, $disk_reference->{SAN},
			    $disk_reference->{SMART}, $hdparm_version, $disk_reference->{sampler_result},
			    $disk_reference->{sampler_version});
	    $sth = $dbh->prepare($statement_prototype);
	    $sth->execute(@bind_values);
	}

	$statement_prototype = q(DELETE FROM pending WHERE serial = ?);
	$sth = $dbh->prepare($statement_prototype);
	$sth->execute($system_ref->{serial});
	$dbh->commit;
	$dbh->disconnect;
	return $row_id;
    } catch {
	warn "Transaction aborted because $_"; # Try::Tiny copies $@ into $_
	eval { $dbh->rollback };
	die "Transaction failed with message: $!";
    }
}




sub ticking_clock{
    #This does nothing, ignore entirely
    local $| = 1;

    my $seconds_passed = 1;
    while ('true'){
	#my $hours = int($seconds_passed / 3600);
	my $minutes = int($seconds_passed / 60);
	my $seconds = int($seconds_passed % 60);
	my $output_string = "Elapsed time: " . $minutes . " Minutes " . $seconds . " Seconds.";
	print $output_string;
	$seconds_passed = $seconds_passed + 1;
	usleep(100000);
	print ("\b" x length($output_string));
    }
}

sub network_configurator{
    opendir(my $net_dir, '/sys/class/net');
    my @wireless_interfaces;
    my @ethernet_interfaces;
    while (my $file = readdir($net_dir)){
        push(@wireless_interfaces, $file) if ($file =~ /^w/);
	push(@ethernet_interfaces, $file) if ($file =~ /^e/);
    }	
    closedir($net_dir);

    foreach my $wlan (@wireless_interfaces){
	#system(EXIT_ANY, "wpa_supplicant -i $wlan -c /etc/wpa_supplicant/wpa_supplicant.conf -B");
	while('true'){
	    system(EXIT_ANY, "iwctl station $wlan scan");
	    if ($EXITVAL != 0){
		say "iwd appears to have failed scan, wireless card may still be dead, retrying...";
		sleep 5;
		next;
	    }
	    else{ last }
	}
	my @get_networks = capture("iwctl station $wlan get-networks");
	@get_networks = grep { !/^-/ && !/^\s+Network name/ && !/^\s+Available networks/ } @get_networks;
	s/\s+// for @get_networks;
	my @wireless_networks;
	push (@wireless_networks, (split(/\s+/, $_))[0]) for @get_networks;

	while ('true'){
	    system("iwctl station $wlan get-networks");
	    print 'Please select a network from the list above: ';
	    my $answer = <STDIN>;
	    chomp $answer;
	    if ($answer eq 'forget'){
		print "Which network do you wish to forget: ";
		my $target_network = <STDIN>;
		chomp $target_network;
		system(EXIT_ANY, "iwctl known-networks ${target_network} forget");
		next;
	    }
	    if (grep { /$answer/ } @wireless_networks){
		system(EXIT_ANY, "iwctl station $wlan connect $answer");
		if ($EXITVAL == 0){ last }
		else{
		    say "Invalid password";
		    next;
		}
	    }
	    else { next }
	}
	sleep 5;
	open(my $CARRIER, '<', "/sys/class/net/$wlan/carrier");
	my $carrier_read = <$CARRIER>;
	chomp $carrier_read;
	close($CARRIER);
	if ($carrier_read == 1) {
	    system("dhcpcd -n $wlan 2> /dev/null");
	    if ($EXITVAL == 0){ return 'wireless' }
	    else {
		say "DHCP configuration failed";
		sleep 1;
		next;
	    }
	}
	else { next }
    }
    
    foreach my $eth (@ethernet_interfaces){
	while ('true'){
	    system(EXIT_ANY, "pkill dhcpcd");
	    system(EXIT_ANY, "dhcpcd -n $eth");
	    if ($EXITVAL != 0){	    
		say "WARNING: The ethernet port on this system is either malfunctioning or disconnected";
		say "Please check the cable connection and press enter...";
		<STDIN>;
	    }
	    else{ last }
	}
	system(EXIT_ANY, "dhcpcd -n $eth");
	if ($EXITVAL == 0) { return 'ethernet' }
	else { die "dhcpcd failed to connect" }
    }
}

sub check_updates{
    #This subroutine is intentionally disabled, it's broken as all hell
    #Should this be a relative path?
    chdir("triage");
    my @git_status = capture("git pull --dry-run 2>&1");
    if (@git_status){
	system("git pull");
	chdir("..");
	exec("perl triage/triage.perl");
    }
    else{ chdir("..") }
}

