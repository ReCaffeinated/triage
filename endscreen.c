/*Triage Endscreen version 0.8*/

#include <ncurses.h>
#include <unistd.h>
#include <signal.h>

//int const true = 1;
//int const false = 0;

int main ( int argc, char *argv[] ){
  signal(3, SIG_IGN); //Ignore SIGQUIT
  int colorPair = 3;
  int ypos;
  int xpos;
  char lastCharacter;

  int booleanArray[1024];
  for(int i = 0; i < 1024; i++)
    booleanArray[i] = true;
  for(int i = '0'; i <= '9'; i++)
    booleanArray[i] = false;
  /*for(int i = 'A'; i <= 'Z'; i++)
    booleanArray[i] = false;*/
  for(int i = 'a'; i <= 'z'; i++)
    booleanArray[i] = false;
  for(int i = '['; i <= ']'; i++)
    booleanArray[i] = false;
  booleanArray[' '] = false;
  booleanArray[';'] = false;
  booleanArray['='] = false;
  booleanArray['`'] = false;
  
  
  initscr();
  if (has_colors() == true)
    start_color();
  
  init_pair(1,COLOR_BLUE, COLOR_RED);
  init_pair(2,COLOR_GREEN, COLOR_MAGENTA);
  init_pair(3,COLOR_YELLOW, COLOR_BLUE);
  init_pair(4,COLOR_RED, COLOR_CYAN);
  init_pair(5,COLOR_MAGENTA, COLOR_GREEN);
  init_pair(6,COLOR_BLUE, COLOR_YELLOW);
  init_pair(7,COLOR_BLACK, COLOR_WHITE);
  init_pair(8,COLOR_WHITE, COLOR_BLACK);

  printw("LCD & Keyboard Test\n");
  printw("Press tab to change colors. Press enter to exit\n\n");
  printw("Serial Number: %s\n", argv[1]);
  wbkgd(stdscr, COLOR_PAIR(colorPair));
  refresh();

  while(true){
    lastCharacter = getchar();
    if ( lastCharacter == '\r'  ){
      printw("\nYou still need to press: ");
      if (booleanArray[' '] == false)
	printw("[SPACE]");
      for (int i = 0; i < 1024; i++){
	if (booleanArray[i] == false)
	  printw("%c", i);
      }
      printw("\nAre you sure you wish to exit? [Y\\n] ");
      refresh();
      lastCharacter = getchar();
      if (lastCharacter == 'y' || lastCharacter == 'Y' || lastCharacter == '\r'){
	endwin();
	return 0;
      } else {
	printw("%c", lastCharacter);
	booleanArray[(int)lastCharacter] = true;
      }	
    } else if ( lastCharacter == '\t'){
      if ( colorPair++ == 8 )
	colorPair=1;
      wbkgd(stdscr, COLOR_PAIR(colorPair));
      usleep(100000);
    } else if ( lastCharacter == '\b' || lastCharacter == 127){
      getyx(stdscr, ypos, xpos);
      xpos--;
      mvwdelch(stdscr, ypos, xpos);
    } else {
      printw("%c", lastCharacter);
      booleanArray[(int)lastCharacter] = true;
    }	
    refresh();
  }
}
